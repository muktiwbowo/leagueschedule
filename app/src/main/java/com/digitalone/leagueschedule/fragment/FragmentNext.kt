package com.digitalone.leagueschedule.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.adapter.AdapterNext
import com.digitalone.leagueschedule.controller.ControllerApi
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.presenter.PresenterNext
import com.digitalone.leagueschedule.view.ViewNext
import org.jetbrains.anko.support.v4.ctx

class FragmentNext : Fragment(), ViewNext, SwipeRefreshLayout.OnRefreshListener {

    private var nexList: MutableList<ModelEvent> = mutableListOf()
    private lateinit var recyclerView: RecyclerView
    private lateinit var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit var presenterNext: PresenterNext
    private lateinit var adapterNext: AdapterNext
    private lateinit var leagueId: String

    fun newInstance(): FragmentNext {
        return FragmentNext()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_next, container, false)
        recyclerView = view.findViewById(R.id.view_recycle_next) as RecyclerView
        swipeRefreshLayout = view.findViewById(R.id.view_refresh_next) as SwipeRefreshLayout
        initViews()
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        leagueId = arguments?.getString("leagueId").toString()

    }

    private fun initViews() {
        recyclerView.layoutManager = LinearLayoutManager(ctx)
        adapterNext = AdapterNext(nexList)
        recyclerView.adapter = adapterNext
        presenterNext = PresenterNext(this, ControllerResponse())
        swipeRefreshLayout.setOnRefreshListener(this)
        presenterNext.getNextList(leagueId)
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showNext(nextList: Collection<ModelEvent>) {
        swipeRefreshLayout.isRefreshing = false
        nexList.clear()
        nexList.addAll(nextList)
        adapterNext.notifyDataSetChanged()
    }

    override fun onRefresh() {
        swipeRefreshLayout.isRefreshing = true
        nexList.clear()
        adapterNext.notifyDataSetChanged()
        presenterNext.getNextList(leagueId)
    }
}