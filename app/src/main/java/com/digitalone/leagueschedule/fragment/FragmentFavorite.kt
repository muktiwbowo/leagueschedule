package com.digitalone.leagueschedule.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.adapter.AdapterFavorite
import com.digitalone.leagueschedule.model.ModelFavorite
import com.digitalone.leagueschedule.presenter.PresenterFavorite
import com.digitalone.leagueschedule.view.ViewFavorite
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.ctx
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.yesButton

class FragmentFavorite : Fragment(), ViewFavorite, SwipeRefreshLayout.OnRefreshListener, AdapterFavorite.FavoriteListener {

    private var listFavorite: MutableList<ModelFavorite> = mutableListOf()
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout
    private lateinit var mPresenterFavorite: PresenterFavorite
    private lateinit var mAdapterFavorite: AdapterFavorite
    private lateinit var mStatus: TextView

    fun newInstance(): FragmentFavorite {
        return FragmentFavorite()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_favorite, container, false)
        mRecyclerView = view.findViewById(R.id.view_recycle_favorit)
        mSwipeRefreshLayout = view.findViewById(R.id.view_refresh_favorit)
        mStatus = view.findViewById(R.id.view_favorite_status)
        initViews()
        showEmptyData()
        return view
    }

    private fun initViews() {
        mRecyclerView.layoutManager = LinearLayoutManager(ctx)
        mAdapterFavorite = AdapterFavorite(listFavorite)
        mRecyclerView.adapter = mAdapterFavorite
        mPresenterFavorite = PresenterFavorite(this, ctx)
        mPresenterFavorite.getFavorite()
        mSwipeRefreshLayout.setOnRefreshListener(this)
        mAdapterFavorite.setFavoriteLongListener(this)
        mSwipeRefreshLayout.onRefresh {
            mPresenterFavorite.getFavorite()
        }
    }

    override fun onFavoriteDeleted(position: Int) {
        val modelFavorite = mAdapterFavorite.favorite.get(position)
        alert("Are you sure to delete this favorite ??") {
            title = "Delete Favorite"
            yesButton {
                mPresenterFavorite.deleteFavorite(modelFavorite.idFavorite)
                mSwipeRefreshLayout.isRefreshing = true
                listFavorite.clear()
                mAdapterFavorite.notifyDataSetChanged()
                mPresenterFavorite.getFavorite()
            }
            noButton { }
        }.show()
    }

    fun showEmptyData() {
        if (listFavorite.isEmpty()) {
            mStatus.visibility = View.VISIBLE
        }
    }

    override fun showLoading() {
        mSwipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        mSwipeRefreshLayout.isRefreshing = false
    }

    override fun showFavorite(prevList: Collection<ModelFavorite>) {
        mSwipeRefreshLayout.isRefreshing = false
        listFavorite.clear()
        listFavorite.addAll(prevList)
        mAdapterFavorite.notifyDataSetChanged()
    }

    override fun onRefresh() {
        mSwipeRefreshLayout.isRefreshing = true
        listFavorite.clear()
        mAdapterFavorite.notifyDataSetChanged()
        mPresenterFavorite.getFavorite()
    }
}
