package com.digitalone.leagueschedule.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.adapter.AdapterPrev
import com.digitalone.leagueschedule.controller.ControllerApi
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.presenter.PresenterPrev
import com.digitalone.leagueschedule.view.ViewPrev
import org.jetbrains.anko.support.v4.ctx

class FragmentPrev: Fragment(), ViewPrev, SwipeRefreshLayout.OnRefreshListener {
    private             var preList: MutableList<ModelEvent> = mutableListOf()
    private lateinit    var recyclerView: RecyclerView
    private lateinit    var swipeRefreshLayout: SwipeRefreshLayout
    private lateinit    var presenterPrev: PresenterPrev
    private lateinit    var adapterPrev: AdapterPrev
    private lateinit    var leagueId: String

    fun newInstance(): FragmentPrev{
        return FragmentPrev()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_prev, container, false)
        recyclerView = view.findViewById(R.id.view_recycle_prev) as RecyclerView
        swipeRefreshLayout = view.findViewById(R.id.view_refresh_prev) as SwipeRefreshLayout
        initViews()
        return view
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        leagueId = arguments?.getString("leagueId").toString()
    }

    private fun initViews(){
        recyclerView.layoutManager = LinearLayoutManager(ctx)
        adapterPrev = AdapterPrev(preList)
        recyclerView.adapter = adapterPrev
        presenterPrev = PresenterPrev(this, ControllerResponse())
        presenterPrev.getPrevList(leagueId)
        swipeRefreshLayout.setOnRefreshListener(this)
    }

    override fun showLoading() {
        swipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        swipeRefreshLayout.isRefreshing = false
    }

    override fun showPrev(prevList: Collection<ModelEvent>) {
        swipeRefreshLayout.isRefreshing = false
        preList.clear()
        preList.addAll(prevList)
        adapterPrev.notifyDataSetChanged()
    }

    override fun onRefresh() {
        swipeRefreshLayout.isRefreshing = true
        preList.clear()
        adapterPrev.notifyDataSetChanged()
        presenterPrev.getPrevList(leagueId)
    }
}