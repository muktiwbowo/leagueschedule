package com.digitalone.leagueschedule.controller

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.digitalone.leagueschedule.model.ModelFavorite
import org.jetbrains.anko.db.*

class ControllerDatabase(context: Context):
        ManagedSQLiteOpenHelper(context, "favoritedb", null, 1) {
    companion object {
        private var mControllerDatabase: ControllerDatabase? = null

        @Synchronized
        fun getInstance(context: Context): ControllerDatabase{
            if (mControllerDatabase == null){
                mControllerDatabase = ControllerDatabase(context.applicationContext)
            }
            return mControllerDatabase!!
        }
    }
    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(ModelFavorite.TABLE_FAVORITE, true,
                ModelFavorite.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                ModelFavorite.ID_EVENT to TEXT + UNIQUE,
                ModelFavorite.HOME_NAME to TEXT,
                ModelFavorite.HOME_SCORE to TEXT,
                ModelFavorite.DATE_EVENT to TEXT,
                ModelFavorite.AWAY_NAME to TEXT,
                ModelFavorite.AWAY_SCORE to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(ModelFavorite.TABLE_FAVORITE, true)
    }
}
val Context.database: ControllerDatabase
    get() = ControllerDatabase.getInstance(applicationContext)