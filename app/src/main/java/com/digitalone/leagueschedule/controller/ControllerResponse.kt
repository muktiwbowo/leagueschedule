package com.digitalone.leagueschedule.controller

import android.util.Log
import com.digitalone.leagueschedule.model.LeagueResponse
import com.digitalone.leagueschedule.model.NextResponse
import com.digitalone.leagueschedule.model.PrevResponse
import com.digitalone.leagueschedule.model.SearchResponse
import com.digitalone.leagueschedule.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ControllerResponse {
    fun getPrevMatch(leagueId: String, viewPrev: ViewPrev){
        viewPrev.showLoading()
        ControllerApi().retrofitInstance().getPrevMatch(leagueId).enqueue(object : Callback<PrevResponse> {
            override fun onResponse(call: Call<PrevResponse>, response: Response<PrevResponse>) {
                val listPrev = response.body()?.prev
                listPrev?.let { viewPrev.showPrev(it) }
                viewPrev.hideLoading()
            }

            override fun onFailure(call: Call<PrevResponse>, t: Throwable) {
                Log.e("PresenterPrev","onFailure")
            }
        })
    }

    fun getNextMatch(leagueId: String, viewNext: ViewNext){
        viewNext.showLoading()
        ControllerApi().retrofitInstance().getNextMatch(leagueId).enqueue(object : Callback<NextResponse>{
            override fun onResponse(call: Call<NextResponse>, response: Response<NextResponse>) {
                val listNext = response.body()?.next
                listNext?.let { viewNext.showNext(it) }
                viewNext.hideLoading()
            }

            override fun onFailure(call: Call<NextResponse>, t: Throwable) {

            }
        })
    }

    fun getDetailLeague(leagueId: String, viewDetailLeague: ViewDetailLeague){
        ControllerApi().retrofitInstance().getLeagueDetail(leagueId).enqueue(object : Callback<LeagueResponse>{
            override fun onResponse(call: Call<LeagueResponse>, response: Response<LeagueResponse>) {
                val leagueDetail = response.body()?.leagues
                leagueDetail?.let { viewDetailLeague.showDetailLeague(it) }
            }

            override fun onFailure(call: Call<LeagueResponse>, t: Throwable) {
                Log.e("PresenterDetailLeague", "onFailure"+t.cause)
            }
        })
    }

    fun getSearch(keyword: String, viewSearch: ViewSearch){
        viewSearch.showLoading()
        ControllerApi().retrofitInstance().getSearchMatch(keyword).enqueue(object : Callback<SearchResponse> {
            override fun onResponse(call: Call<SearchResponse>, response: Response<SearchResponse>) {
                val searchList = response.body()?.searchResult
                if (searchList != null) {
                    searchList.let { viewSearch.showSearchResult(it) }
                }else{
                    viewSearch.showSearchStatus()
                }
            }

            override fun onFailure(call: Call<SearchResponse>, t: Throwable) {
            }
        })
    }

    fun getDetail(id: String, viewDetail: ViewDetail){
        ControllerApi().retrofitInstance().getDetailMatch(id).enqueue(object : Callback<NextResponse>{
            override fun onResponse(call: Call<NextResponse>, response: Response<NextResponse>) {
                val detail = response.body()?.next
                detail?.let { viewDetail.populateDetail(detail) }
            }

            override fun onFailure(call: Call<NextResponse>, t: Throwable) {

            }
        })
    }
}