package com.digitalone.leagueschedule.controller

import com.digitalone.leagueschedule.BuildConfig
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ControllerApi {

    fun retrofitInstance(): ControllerRequest{
        val retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        return retrofit.create(ControllerRequest::class.java)
    }
}