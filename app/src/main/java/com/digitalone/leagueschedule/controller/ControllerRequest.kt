package com.digitalone.leagueschedule.controller

import com.digitalone.leagueschedule.model.LeagueResponse
import com.digitalone.leagueschedule.model.NextResponse
import com.digitalone.leagueschedule.model.PrevResponse
import com.digitalone.leagueschedule.model.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ControllerRequest {
    @GET("eventspastleague.php?")
    fun getPrevMatch(@Query("id")
                     query: String?): Call<PrevResponse>

    @GET("eventsnextleague.php?")
    fun getNextMatch(@Query("id")
                     query: String?): Call<NextResponse>

    @GET("searchevents.php?")
    fun getSearchMatch(
            @Query("e")
            query: String?)
            : Call<SearchResponse>

    @GET("lookupleague.php?")
    fun getLeagueDetail(
            @Query("id")
            leagueId: String?): Call<LeagueResponse>

    @GET("lookupevent.php?")
    fun getDetailMatch(@Query("id") id: String): Call<NextResponse>
}