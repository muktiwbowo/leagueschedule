package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerApi
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.model.SearchResponse
import com.digitalone.leagueschedule.view.ViewSearch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterSearch(val viewSearch: ViewSearch, val controllerResponse: ControllerResponse) {
    fun getSearchResult(keyword: String) {
        controllerResponse.getSearch(keyword, object : ViewSearch{
            override fun showLoading() {
                viewSearch.showLoading()
            }

            override fun showSearchStatus() {
                viewSearch.showSearchStatus()
            }

            override fun showSearchResult(searchList: Collection<ModelEvent>) {
                viewSearch.showSearchResult(searchList)
            }

            override fun hideLoading() {
                viewSearch.hideLoading()
            }
        })
    }
}