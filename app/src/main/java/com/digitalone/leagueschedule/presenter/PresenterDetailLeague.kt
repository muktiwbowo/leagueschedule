package com.digitalone.leagueschedule.presenter

import android.util.Log
import com.digitalone.leagueschedule.controller.ControllerApi
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.LeagueResponse
import com.digitalone.leagueschedule.model.ModelDetailLeague
import com.digitalone.leagueschedule.view.ViewDetailLeague
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterDetailLeague(val viewDetailLeague: ViewDetailLeague, val controllerResponse: ControllerResponse) {
    fun getLeagueDetail(leagueId: String){
        controllerResponse.getDetailLeague(leagueId, object : ViewDetailLeague{
            override fun showDetailLeague(detailLeague: Collection<ModelDetailLeague>) {
                viewDetailLeague.showDetailLeague(detailLeague)
            }

        })
    }
}