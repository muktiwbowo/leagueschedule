package com.digitalone.leagueschedule.presenter

import android.content.Context
import android.database.sqlite.SQLiteConstraintException
import com.digitalone.leagueschedule.controller.database
import com.digitalone.leagueschedule.model.ModelFavorite
import com.digitalone.leagueschedule.view.ViewFavorite
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.db.select

class PresenterFavorite(val viewFavorite: ViewFavorite, val context: Context) {
    fun getFavorite(){
        viewFavorite.showLoading()
        try {
            context.database.use {
                viewFavorite.hideLoading()
                val listFavorite    = select(ModelFavorite.TABLE_FAVORITE)
                val favorite        = listFavorite.parseList(classParser<ModelFavorite>())
                favorite.let {
                    viewFavorite.showFavorite(it)
                }
            }
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
        }
    }

    fun deleteFavorite(id: Int){
        try {
            context.database.use {
                delete(ModelFavorite.TABLE_FAVORITE,
                        "ID_ =  {idFavorite}",
                        "idFavorite" to id)
            }
        }catch (e: SQLiteConstraintException){
            e.localizedMessage
        }
    }
}