package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerApi
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.model.NextResponse
import com.digitalone.leagueschedule.view.ViewNext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PresenterNext(val viewNext: ViewNext, val controllerResponse: ControllerResponse) {
    fun getNextList(leagueId: String) {
        controllerResponse.getNextMatch(leagueId, object : ViewNext{
            override fun hideLoading() {
                viewNext.hideLoading()
            }

            override fun showNext(nextList: Collection<ModelEvent>) {
                viewNext.showNext(nextList)
            }

            override fun showLoading() {
                viewNext.showLoading()
            }

        })
    }
}