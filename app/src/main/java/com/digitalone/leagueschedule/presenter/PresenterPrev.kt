package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.view.ViewPrev

class PresenterPrev(val viewPrev: ViewPrev, val controllerResponse: ControllerResponse) {
    fun getPrevList(leagueId: String){
        controllerResponse.getPrevMatch(leagueId, object : ViewPrev{
            override fun showLoading() {
                viewPrev.showLoading()
            }

            override fun hideLoading() {
                viewPrev.hideLoading()
            }

            override fun showPrev(prevList: Collection<ModelEvent>) {
                viewPrev.showPrev(prevList)
            }

        })
    }
}