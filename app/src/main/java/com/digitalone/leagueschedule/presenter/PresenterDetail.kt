package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.view.ViewDetail

class PresenterDetail (val viewDetail: ViewDetail, val controllerResponse: ControllerResponse){
    fun getDetail(id: String){
        controllerResponse.getDetail(id, object : ViewDetail {
            override fun populateDetail(detail: Collection<ModelEvent>) {
                viewDetail.populateDetail(detail)
            }
        })
    }
}