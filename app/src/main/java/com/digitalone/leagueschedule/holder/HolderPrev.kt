package com.digitalone.leagueschedule.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.digitalone.leagueschedule.activity.ActivityDetail
import com.digitalone.leagueschedule.model.ModelEvent
import kotlinx.android.synthetic.main.holder_prev.view.*

class HolderPrev(view: View): RecyclerView.ViewHolder(view) {
    fun bindItemPrev(prev: ModelEvent){
        itemView.view_prev_date.text        = prev.dateEvent
        itemView.view_prev_name_left.text   = prev.strHomeTeam
        itemView.view_prev_score_left.text  = prev.intHomeScore
        itemView.view_prev_name_right.text  = prev.strAwayTeam
        itemView.view_prev_score_right.text = prev.intAwayScore

        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityDetail::class.java)
            intent.putExtra("idEvent", prev.idEvent)
            itemView.context.startActivity(intent)
        }
    }
}