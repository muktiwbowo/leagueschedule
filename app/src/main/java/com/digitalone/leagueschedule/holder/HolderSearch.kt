package com.digitalone.leagueschedule.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.digitalone.leagueschedule.activity.ActivityDetail
import com.digitalone.leagueschedule.model.ModelEvent
import kotlinx.android.synthetic.main.holder_search.view.*

class HolderSearch(view: View): RecyclerView.ViewHolder(view) {
    fun bindSearch(modelEvent: ModelEvent){
        itemView.view_search_name_left.text     = modelEvent.strHomeTeam
        itemView.view_search_score_left.text    = modelEvent.intHomeScore
        itemView.view_search_name_right.text    = modelEvent.strAwayTeam
        itemView.view_search_score_right.text   = modelEvent.intAwayScore
        itemView.view_search_date.text          = modelEvent.dateEvent
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityDetail::class.java)
            intent.action = "detail_search"
            intent.putExtra("search",modelEvent)
            itemView.context.startActivity(intent)
        }
    }
}