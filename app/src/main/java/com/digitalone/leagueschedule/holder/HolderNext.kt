package com.digitalone.leagueschedule.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.digitalone.leagueschedule.activity.ActivityDetail
import com.digitalone.leagueschedule.model.ModelEvent
import kotlinx.android.synthetic.main.holder_next.view.*

class HolderNext(view: View): RecyclerView.ViewHolder(view) {
    fun bindNext(next: ModelEvent){
        itemView.view_next_date.text        = next.dateEvent
        itemView.view_next_name_left.text   = next.strHomeTeam
        itemView.view_next_score_left.text  = next.intHomeScore
        itemView.view_next_name_right.text  = next.strAwayTeam
        itemView.view_next_score_right.text = next.intAwayScore
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityDetail::class.java)
            intent.putExtra("idEvent", next.idEvent)
            itemView.context.startActivity(intent)
        }
    }
}