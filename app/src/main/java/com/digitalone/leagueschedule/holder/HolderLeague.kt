package com.digitalone.leagueschedule.holder

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.View
import com.digitalone.leagueschedule.activity.ActivityDetailLeague
import com.digitalone.leagueschedule.model.ModelLeague
import kotlinx.android.synthetic.main.holder_league.view.*

class HolderLeague(view: View): RecyclerView.ViewHolder(view) {
    fun bindLeague(modelLeague: ModelLeague){
        itemView.view_league_name.text = modelLeague.teamName
        itemView.setOnClickListener {
            val intent = Intent(itemView.context, ActivityDetailLeague::class.java)
            intent.putExtra("league", modelLeague)
            itemView.context.startActivity(intent)
        }
    }
}