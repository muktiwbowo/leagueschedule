package com.digitalone.leagueschedule.activity

import android.app.SearchManager
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.adapter.AdapterSearch
import com.digitalone.leagueschedule.controller.ControllerApi
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.presenter.PresenterSearch
import com.digitalone.leagueschedule.view.ViewSearch
import kotlinx.android.synthetic.main.activity_search.*
import org.jetbrains.anko.ctx

class ActivitySearch : AppCompatActivity(), ViewSearch {

    private lateinit    var mPresenterSearch: PresenterSearch
    private             var listSearch: MutableList<ModelEvent> = mutableListOf()
    private lateinit    var mRecycleView: RecyclerView
    private lateinit    var mSwipeRefreshLayout: SwipeRefreshLayout
    private lateinit    var mAdapterSearch: AdapterSearch
    private lateinit    var query: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        getPassingIntent(intent)
        initViews()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun getPassingIntent(intent: Intent?) {
        if (Intent.ACTION_SEARCH == intent?.action) {
            query = intent.getStringExtra(SearchManager.QUERY)
            Log.e("query", ""+query)
        }
    }

    private fun getSearchResult(keyword: String){
        mPresenterSearch.getSearchResult(keyword)
    }

    private fun initViews(){
        supportActionBar?.title = "Search Match"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        mRecycleView                = view_recycle_search
        mSwipeRefreshLayout         = view_refresh_search
        mRecycleView.layoutManager  = LinearLayoutManager(ctx)
        mAdapterSearch = AdapterSearch(listSearch)
        mRecycleView.adapter        = mAdapterSearch
        mPresenterSearch            = PresenterSearch(this, ControllerResponse())
        getSearchResult(query)
    }

    override fun showLoading() {
        mSwipeRefreshLayout.isRefreshing = true
    }

    override fun hideLoading() {
        mSwipeRefreshLayout.isRefreshing = false

    }

    override fun showSearchStatus() {
        view_search_status.visibility = View.VISIBLE
    }

    override fun showSearchResult(searchList: Collection<ModelEvent>) {
        mSwipeRefreshLayout.isRefreshing = false
        listSearch.clear()
        listSearch.addAll(searchList)
        mAdapterSearch.notifyDataSetChanged()
    }
}
