package com.digitalone.leagueschedule.activity

import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.adapter.AdapterLeague
import com.digitalone.leagueschedule.model.ModelLeague
import org.jetbrains.anko.ctx

class ActivityLeague : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {

    private          var leagueList: MutableList<ModelLeague> = mutableListOf()
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout
    private lateinit var mAdapterLeague: AdapterLeague

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_league)

        val leagueId        = resources.getStringArray(R.array.league_id)
        val leagueName      = resources.getStringArray(R.array.league_name)
        mRecyclerView       = findViewById(R.id.view_recycle_league)
        mSwipeRefreshLayout = findViewById(R.id.view_refresh_league)
        for (i in 0..5) {
            val modelLeague = ModelLeague(leagueId[i], leagueName[i])
            leagueList.add(modelLeague)
        }

        mRecyclerView.layoutManager = LinearLayoutManager(ctx)
        mAdapterLeague              = AdapterLeague(leagueList)
        mRecyclerView.adapter       = mAdapterLeague
    }

    override fun onRefresh() {

    }
}
