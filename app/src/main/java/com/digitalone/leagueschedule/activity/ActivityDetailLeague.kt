package com.digitalone.leagueschedule.activity

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.controller.ControllerApi
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.fragment.FragmentFavorite
import com.digitalone.leagueschedule.fragment.FragmentNext
import com.digitalone.leagueschedule.fragment.FragmentPrev
import com.digitalone.leagueschedule.model.ModelDetailLeague
import com.digitalone.leagueschedule.model.ModelLeague
import com.digitalone.leagueschedule.presenter.PresenterDetailLeague
import com.digitalone.leagueschedule.view.ViewDetailLeague
import kotlinx.android.synthetic.main.activity_detail_league.*
import org.jetbrains.anko.ctx

class ActivityDetailLeague : AppCompatActivity(), ViewDetailLeague, BottomNavigationView.OnNavigationItemSelectedListener {

    private lateinit var mSearchView: SearchView
    private lateinit var mModelLeague: ModelLeague
    private lateinit var mPresenterDetailLeague: PresenterDetailLeague
    private          var listDetail: MutableList<ModelDetailLeague> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_league)
        bottom_nav.setOnNavigationItemSelectedListener(this)
        getPassingIntent(intent)
        mPresenterDetailLeague = PresenterDetailLeague(this, ControllerResponse())
        mPresenterDetailLeague.getLeagueDetail(mModelLeague.idLeague.toString())
        if (savedInstanceState == null) {
            loadFragmentPrev()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search, menu)
        val menuItem = menu?.findItem(R.id.search_match)
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        mSearchView = menuItem?.actionView as SearchView
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo(componentName))
        return true
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.prev_match -> loadFragmentPrev()
            R.id.next_match -> loadFragmentNext()
            R.id.favorite_match -> loadFragmentFavorite()
        }
        return true
    }

    override fun showDetailLeague(detailLeague: Collection<ModelDetailLeague>) {
        listDetail.addAll(detailLeague)
        populateViews(listDetail)
    }

    private fun populateViews(listDetail: MutableList<ModelDetailLeague>) {
        for (i in listDetail.indices){
            league_desc.text  = listDetail.get(i).leagueDesc
            Glide.with(ctx).load(listDetail.get(i).leagueBadge).into(league_image)
        }
    }

    fun getPassingIntent(intent: Intent){
        mModelLeague = intent.getParcelableExtra("league")
    }

    private fun loadFragmentPrev() {
        val fragmentPrev = FragmentPrev()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val bundle = Bundle()
        bundle.putString("leagueId", mModelLeague.idLeague)
        fragmentPrev.arguments = bundle
        fragmentPrev.newInstance()
        fragmentTransaction.replace(R.id.frame_layout, fragmentPrev)
        fragmentTransaction.commit()
    }

    private fun loadFragmentNext() {
        val fragmentNext = FragmentNext()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val bundle = Bundle()
        bundle.putString("leagueId", mModelLeague.idLeague)
        fragmentNext.arguments = bundle
        fragmentNext.newInstance()
        fragmentTransaction.replace(R.id.frame_layout, fragmentNext)
        fragmentTransaction.commit()
    }

    private fun loadFragmentFavorite() {
        val fragmentFavorite = FragmentFavorite()
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentFavorite.newInstance()
        fragmentTransaction.replace(R.id.frame_layout, fragmentFavorite)
        fragmentTransaction.commit()
    }
}
