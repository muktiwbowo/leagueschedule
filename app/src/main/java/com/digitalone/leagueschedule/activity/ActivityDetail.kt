package com.digitalone.leagueschedule.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.database.sqlite.SQLiteConstraintException
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.UtilString
import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.controller.database
import com.digitalone.leagueschedule.model.ModelDetailLeague
import com.digitalone.leagueschedule.model.ModelEvent
import com.digitalone.leagueschedule.model.ModelFavorite
import com.digitalone.leagueschedule.presenter.PresenterDetail
import com.digitalone.leagueschedule.view.ViewDetail
import kotlinx.android.synthetic.main.activity_detail.*
import org.jetbrains.anko.ctx
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.toast

class ActivityDetail : AppCompatActivity(), ViewDetail {

    private lateinit    var mEvent: ModelEvent
    private lateinit var idEvent: String
    private lateinit var presenterDetail: PresenterDetail
    private          var listDetail: MutableList<ModelEvent> = mutableListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        getPassingIntent(intent)
        presenterDetail = PresenterDetail(this, ControllerResponse())
        presenterDetail.getDetail(idEvent)
        initToolbar()
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.favorite, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.favorite -> saveFavorite()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initToolbar(){
        supportActionBar?.title = "Match Detail"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun getPassingIntent(intent: Intent?){
        idEvent = intent?.getStringExtra("idEvent").toString()
        Log.e("tes", ""+idEvent)
    }

    private fun saveFavorite(){
        try {
            database.use {
                insert(
                        ModelFavorite.TABLE_FAVORITE,
                        ModelFavorite.ID_EVENT to mEvent.idEvent,
                        ModelFavorite.HOME_NAME to mEvent.strHomeTeam,
                        ModelFavorite.HOME_SCORE to mEvent.intHomeScore,
                        ModelFavorite.AWAY_NAME to mEvent.strAwayTeam,
                        ModelFavorite.AWAY_SCORE to mEvent.intAwayScore,
                        ModelFavorite.DATE_EVENT to mEvent.dateEvent
                )
            }
            toast("successfully inserted")
        }catch(e: SQLiteConstraintException) {
            toast("failed to insert favorit")
            e.localizedMessage
        }
    }

    override fun populateDetail(detail: Collection<ModelEvent>) {
        listDetail.addAll(detail)
        populateView(listDetail)
    }

    @SuppressLint("SetTextI18n")
    private fun populateView(mEvent: List<ModelEvent>){
        for (i in mEvent.indices) {
            view_detail_date.text = "Date: " + UtilString.checkString(mEvent.get(i).dateEvent)
            view_detail_name_left.text = UtilString.checkString(mEvent.get(i).strHomeTeam)
            view_detail_score_left.text = UtilString.checkString(mEvent.get(i).intHomeScore)
            view_left_goal_detail.text = "Goal Detail: " + UtilString.checkString(mEvent.get(i).strHomeGoalDetails)
            view_left_redcard.text = "Red Card: " + UtilString.checkString(mEvent.get(i).strHomeRedCards)
            view_left_yellowcard.text = "Yellow Card: " + UtilString.checkString(mEvent.get(i).strHomeYellowCards)
            view_left_lineup_defense.text = "Defense: " + UtilString.checkString(mEvent.get(i).strHomeLineupDefense)
            view_left_lineup_goalkeeper.text = "Goal Keeper: " + UtilString.checkString(mEvent.get(i).strHomeLineupGoalkeeper)
            view_left_lineup_midfield.text = "Mid Field: " + UtilString.checkString(mEvent.get(i).strHomeLineupMidfield)
            view_left_lineup_forward.text = "Forward: " + UtilString.checkString(mEvent.get(i).strHomeLineupForward)
            view_left_subtitutes.text = "Subtitutes: " + UtilString.checkString(mEvent.get(i).strHomeLineupSubstitutes)
            view_detail_name_right.text = UtilString.checkString(mEvent.get(i).strAwayTeam)
            view_detail_score_right.text = UtilString.checkString(mEvent.get(i).intAwayScore)
            view_right_goal_detail.text = "Goal Detail: " + UtilString.checkString(mEvent.get(i).strAwayGoalDetails)
            view_right_redcard.text = "Red Card: " + UtilString.checkString(mEvent.get(i).strAwayRedCards)
            view_right_yellowcard.text = "Yellow Card: " + UtilString.checkString(mEvent.get(i).strAwayYellowCards)
            view_right_lineup_defense.text = "Defense: " + UtilString.checkString(mEvent.get(i).strAwayLineupDefense)
            view_right_lineup_goalkeeper.text = "Goal Keeper: " + UtilString.checkString(mEvent.get(i).strAwayLineupGoalkeeper)
            view_right_lineup_midfield.text = "Mid Field: " + UtilString.checkString(mEvent.get(i).strAwayLineupMidfield)
            view_right_lineup_forward.text = "Forward: " + UtilString.checkString(mEvent.get(i).strAwayLineupForward)
            view_right_subtitutes.text = "Subtitutes: " + UtilString.checkString(mEvent.get(i).strAwayLineupSubstitutes)
            if (mEvent.get(i).strThumb.isNullOrEmpty()) {
                view_detail_placeholder.visibility = View.VISIBLE
            } else {
                Glide.with(ctx).load(mEvent.get(i).strThumb)
                        .centerCrop()
                        .into(view_detail_image.findViewById(R.id.view_detail_image))
            }
        }
    }
}
