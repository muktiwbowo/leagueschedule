package com.digitalone.leagueschedule.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelFavorite (
        @SerializedName("ID_")
        var idFavorite: Int,
        @SerializedName("ID_EVENT")
        var idEvent: String,
        @SerializedName("DATE_EVENT")
        var dateEvent: String?,
        @SerializedName("HOME_NAME")
        var homeTeam: String?,
        @SerializedName("HOME_SCORE")
        var homeScore: String?,
        @SerializedName("AWAY_NAME")
        var awayTeam: String?,
        @SerializedName("AWAY_SCORE")
        var awayScore: String?
): Parcelable {
        companion object {
                const val TABLE_FAVORITE: String = "FAVORITE_MATCH"
                const val ID: String = "ID_"
                const val ID_EVENT: String = "ID_EVENT"
                const val HOME_NAME: String = "HOME_NAME"
                const val HOME_SCORE: String = "HOME_SCORE"
                const val DATE_EVENT: String = "DATE_EVENT"
                const val AWAY_SCORE: String = "AWAY_SCORE"
                const val AWAY_NAME: String = "AWAY_NAME"
        }
}