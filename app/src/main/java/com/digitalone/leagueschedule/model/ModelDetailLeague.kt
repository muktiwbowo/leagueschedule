package com.digitalone.leagueschedule.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class ModelDetailLeague(
        @SerializedName("strLeague")
        var leagueName: String?,
        @SerializedName("strDescriptionEN")
        var leagueDesc: String?,
        @SerializedName("strBadge")
        var leagueBadge: String?
): Parcelable