package com.digitalone.leagueschedule.model

import com.google.gson.annotations.SerializedName

data class LeagueResponse (@SerializedName("leagues") val leagues: Collection<ModelDetailLeague>)