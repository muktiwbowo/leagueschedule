package com.digitalone.leagueschedule.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NextResponse (
        @SerializedName("events")
        val next: List<ModelEvent>) : Parcelable