package com.digitalone.leagueschedule.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ModelLeague(
        var idLeague: String?,
        var teamName: String?
): Parcelable