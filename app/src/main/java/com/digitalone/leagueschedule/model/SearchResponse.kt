package com.digitalone.leagueschedule.model

import com.google.gson.annotations.SerializedName

data class SearchResponse(
        @SerializedName("event")
        var searchResult: ArrayList<ModelEvent>
)