package com.digitalone.leagueschedule.view

import com.digitalone.leagueschedule.model.ModelFavorite


interface ViewFavorite {
    fun showLoading()
    fun hideLoading()
    fun showFavorite(prevList: Collection<ModelFavorite>)
}