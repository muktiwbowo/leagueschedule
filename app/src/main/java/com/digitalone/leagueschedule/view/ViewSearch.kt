package com.digitalone.leagueschedule.view

import com.digitalone.leagueschedule.model.ModelEvent

interface ViewSearch {
    fun showLoading()
    fun hideLoading()
    fun showSearchStatus()
    fun showSearchResult(searchList: Collection<ModelEvent>)
}