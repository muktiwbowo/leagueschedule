package com.digitalone.leagueschedule.view

import com.digitalone.leagueschedule.model.ModelEvent

interface ViewDetail {
    fun populateDetail(detail: Collection<ModelEvent>)
}