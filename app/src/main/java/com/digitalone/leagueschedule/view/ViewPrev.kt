package com.digitalone.leagueschedule.view

import com.digitalone.leagueschedule.model.ModelEvent

interface ViewPrev {
    fun showLoading()
    fun hideLoading()
    fun showPrev(prevList: Collection<ModelEvent>)
}