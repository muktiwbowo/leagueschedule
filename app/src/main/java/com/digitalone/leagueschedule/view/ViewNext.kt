package com.digitalone.leagueschedule.view

import com.digitalone.leagueschedule.model.ModelEvent

interface ViewNext {
    fun showLoading()
    fun hideLoading()
    fun showNext(nextList: Collection<ModelEvent>)
}