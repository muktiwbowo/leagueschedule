package com.digitalone.leagueschedule.view

import com.digitalone.leagueschedule.model.ModelDetailLeague

interface ViewDetailLeague {
    fun showDetailLeague(detailLeague: Collection<ModelDetailLeague>)
}