package com.digitalone.leagueschedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.holder.HolderFavorite
import com.digitalone.leagueschedule.model.ModelFavorite

class AdapterFavorite(val favorite: List<ModelFavorite>): RecyclerView.Adapter<HolderFavorite>() {
    private lateinit var mFavoriteLitener: FavoriteListener

    interface FavoriteListener{
        fun onFavoriteDeleted(position: Int)
    }

    fun setFavoriteLongListener(favoriteListener: FavoriteListener){
        mFavoriteLitener    = favoriteListener
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderFavorite {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.holder_favorite, parent, false)
        val holderFavorite = HolderFavorite(view)
        holderFavorite.setFavoriteListener(mFavoriteLitener)
        return holderFavorite
    }

    override fun getItemCount() = favorite.size

    override fun onBindViewHolder(holder: HolderFavorite, position: Int) {
        holder.bindFavorite(favorite[position])
    }
}