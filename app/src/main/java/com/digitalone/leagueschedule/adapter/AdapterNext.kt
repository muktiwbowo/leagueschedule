package com.digitalone.leagueschedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.holder.HolderNext
import com.digitalone.leagueschedule.model.ModelEvent

class AdapterNext(val next: List<ModelEvent>): RecyclerView.Adapter<HolderNext>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderNext {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.holder_next, parent, false)
        return HolderNext(view)
    }

    override fun getItemCount() = next.size

    override fun onBindViewHolder(holder: HolderNext, position: Int) {
        holder.bindNext(next[position])
    }
}