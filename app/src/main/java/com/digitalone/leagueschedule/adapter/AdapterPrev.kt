package com.digitalone.leagueschedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.holder.HolderPrev
import com.digitalone.leagueschedule.model.ModelEvent

class AdapterPrev(private val prev: List<ModelEvent>): RecyclerView.Adapter<HolderPrev>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderPrev {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.holder_prev, parent, false)
        return HolderPrev(view)
    }

    override fun getItemCount() = prev.size

    override fun onBindViewHolder(holder: HolderPrev, position: Int) {
        holder.bindItemPrev(prev[position])
    }
}