package com.digitalone.leagueschedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.holder.HolderSearch
import com.digitalone.leagueschedule.model.ModelEvent

class AdapterSearch(val searchList: List<ModelEvent>): RecyclerView.Adapter<HolderSearch>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderSearch {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.holder_search, parent, false)
        return HolderSearch(view)
    }

    override fun getItemCount(): Int = searchList.size

    override fun onBindViewHolder(holder: HolderSearch, position: Int) {
        holder.bindSearch(searchList[position])
    }
}