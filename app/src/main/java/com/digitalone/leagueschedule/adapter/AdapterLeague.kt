package com.digitalone.leagueschedule.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.digitalone.leagueschedule.R
import com.digitalone.leagueschedule.holder.HolderLeague
import com.digitalone.leagueschedule.model.ModelLeague

class AdapterLeague(val listLeague: List<ModelLeague>): RecyclerView.Adapter<HolderLeague>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderLeague {
        val view = LayoutInflater.from(p0.context)
                .inflate(R.layout.holder_league, p0, false)
        return HolderLeague(view)
    }

    override fun getItemCount(): Int = listLeague.size

    override fun onBindViewHolder(p0: HolderLeague, p1: Int) {
        p0.bindLeague(listLeague[p1])
    }
}