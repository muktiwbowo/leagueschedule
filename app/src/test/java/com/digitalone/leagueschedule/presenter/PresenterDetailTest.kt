package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.NextResponse
import com.digitalone.leagueschedule.view.ViewDetail
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PresenterDetailTest {
    @Mock
    private lateinit var viewDetail: ViewDetail
    @Mock
    private lateinit var controllerResponse: ControllerResponse
    @Mock
    private lateinit var nextResponse: NextResponse
    private lateinit var presenterDetail: PresenterDetail

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenterDetail = PresenterDetail(viewDetail, controllerResponse)
    }

    @Test
    fun getDetail(){
        val id = "579384"

        presenterDetail.getDetail(id)

        argumentCaptor<ViewDetail>().apply {
            verify(controllerResponse).getDetail(eq(id), capture())
            firstValue.populateDetail(nextResponse.next)
        }

        verify(viewDetail).populateDetail(nextResponse.next)
    }
}