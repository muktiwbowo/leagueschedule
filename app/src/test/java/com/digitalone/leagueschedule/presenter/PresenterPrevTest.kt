package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.PrevResponse
import com.digitalone.leagueschedule.view.ViewPrev
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import org.junit.Test

import org.junit.Before
import org.mockito.*

class PresenterPrevTest {

    @Mock
    private lateinit var controllerResponse: ControllerResponse
    @Mock
    private lateinit var viewPrev: ViewPrev
    @Mock
    private lateinit var prevResponse: PrevResponse

    private lateinit var presenterPrev: PresenterPrev


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenterPrev = PresenterPrev(viewPrev, controllerResponse)
    }

    @Test
    fun getPrevList() {
        val leagueId    = "4331"
        viewPrev.showLoading()
        presenterPrev.getPrevList(leagueId)
        viewPrev.hideLoading()

        argumentCaptor<ViewPrev>().apply {
            verify(controllerResponse).getPrevMatch(eq(leagueId), capture())
            firstValue.showPrev(prevResponse.prev)
        }

        verify(viewPrev).showLoading()
        verify(viewPrev).showPrev(prevResponse.prev)
        verify(viewPrev).hideLoading()
    }
}