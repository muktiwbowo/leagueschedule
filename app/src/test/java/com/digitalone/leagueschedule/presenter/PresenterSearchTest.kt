package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.SearchResponse
import com.digitalone.leagueschedule.view.ViewSearch
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PresenterSearchTest {
    @Mock
    private lateinit var viewSearch: ViewSearch
    @Mock
    private lateinit var controllerResponse: ControllerResponse
    @Mock
    private lateinit var searchResponse: SearchResponse
    private lateinit var presenterSearch: PresenterSearch

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenterSearch = PresenterSearch(viewSearch, controllerResponse)
    }

    @Test
    fun getSearch(){
        val keyword = "liverpool"
        presenterSearch.getSearchResult(keyword)

        argumentCaptor<ViewSearch>().apply {
            verify(controllerResponse).getSearch(eq(keyword), capture())
            firstValue.showSearchResult(searchResponse.searchResult)
        }

        verify(viewSearch).showSearchResult(searchResponse.searchResult)
    }
}