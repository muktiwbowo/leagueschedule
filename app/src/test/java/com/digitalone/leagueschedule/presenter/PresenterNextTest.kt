package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.NextResponse
import com.digitalone.leagueschedule.model.PrevResponse
import com.digitalone.leagueschedule.view.ViewNext
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PresenterNextTest {
    @Mock
    private lateinit var controllerResponse: ControllerResponse
    @Mock
    private lateinit var viewNext: ViewNext
    @Mock
    private lateinit var nextResponse: NextResponse
    private lateinit var presenterNext: PresenterNext

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenterNext   = PresenterNext(viewNext, controllerResponse)
    }

    @Test
    fun getNextMatch(){
        val leagueId = "4331"

        viewNext.showLoading()
        presenterNext.getNextList(leagueId)
        viewNext.hideLoading()
        argumentCaptor<ViewNext>().apply {
            verify(controllerResponse).getNextMatch(eq(leagueId), capture())
            firstValue.showNext(nextResponse.next)
        }
        verify(viewNext).showLoading()
        verify(viewNext).showNext(nextResponse.next)
        verify(viewNext).hideLoading()
    }
}