package com.digitalone.leagueschedule.presenter

import com.digitalone.leagueschedule.controller.ControllerResponse
import com.digitalone.leagueschedule.model.LeagueResponse
import com.digitalone.leagueschedule.view.ViewDetailLeague
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.eq
import com.nhaarman.mockito_kotlin.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class PresenterDetailLeagueTest {
    @Mock
    private lateinit var viewDetailLeague: ViewDetailLeague
    @Mock
    private lateinit var controllerResponse: ControllerResponse
    @Mock
    private lateinit var leagueResponse: LeagueResponse
    private lateinit var presenterDetailLeague: PresenterDetailLeague

    @Before
    fun setUp(){
        MockitoAnnotations.initMocks(this)
        presenterDetailLeague = PresenterDetailLeague(viewDetailLeague, controllerResponse)
    }

    @Test
    fun getDetailLeague(){
        val leagueId = "4331"
        presenterDetailLeague.getLeagueDetail(leagueId)

        argumentCaptor<ViewDetailLeague>().apply {
            verify(controllerResponse).getDetailLeague(eq(leagueId), capture())
            firstValue.showDetailLeague(leagueResponse.leagues)
        }

        verify(viewDetailLeague).showDetailLeague(leagueResponse.leagues)
    }
}